use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::{Arc, Mutex};

use biquad::DEFAULT_Q;
use nih_plug::prelude::*;
use nih_plug_egui::egui::Vec2;
use nih_plug_egui::{create_egui_editor, egui, EguiState};

use saturation::{Hysteresis, Saturation};
use spectrum::{SpectrumInput, SpectrumOutput};
use starlight_audio_dsp::{*};
use starlight_audio_widgets::response_graph::ResponseGraph;
use starlight_audio_widgets::slider::Slider;
use starlight_audio_widgets::{dial::Dial, tone_control::ToneControl};

const MAX_BLOCK_SIZE: usize = 32;
const OVERSAMPLING_FACTOR: usize = 3;  // 8x
const OVERSAMPLING_TIMES: usize = oversampling_factor_to_times(OVERSAMPLING_FACTOR);
const MAX_OVERSAMPLED_BLOCK_SIZE: usize = MAX_BLOCK_SIZE * OVERSAMPLING_TIMES;

const MIN_FILTER_FREQUENCY: f32 = 20.0;
const MAX_FILTER_FREQUENCY: f32 = 20000.0;

const MIN_FILTER_GAIN: f32 = -3.0;
const MAX_FILTER_GAIN: f32 = 6.0;

struct Nebula {
    params: Arc<NebulaParams>,
    sample_rate: Arc<AtomicF32>,
    num_channels: Arc<AtomicUsize>,

    buffers: Box<ScratchBuffers>, // Buffers for oversampled smoothed param values

    oversamplers: Vec<oversampling::Lanczos3Oversampler>,

    input_copies: Vec<[f32; MAX_BLOCK_SIZE]>,

    hysteresises: Vec<Hysteresis>,
    saturation_effects: Vec<Saturation>,

    high_pass_filters: Vec<biquad::Biquad>,
    high_pass_coefficients: biquad::BiquadCoefficients,
    prev_high_pass_freq: f32,

    peak_filters: Vec<biquad::Biquad>,
    peak_coefficients: biquad::BiquadCoefficients,
    prev_peak_freq: f32,
    prev_peak_gain: f32,

    low_pass_filters: Vec<biquad::Biquad>,
    low_pass_coefficients: biquad::BiquadCoefficients,
    prev_low_pass_freq: f32,

    dc_filters: Vec<biquad::Biquad>,
    dc_filter_coefficients: biquad::BiquadCoefficients,

    mono_spectrum_input: SpectrumInput,
    mono_spectrum_output: Arc<Mutex<SpectrumOutput>>,
    stereo_spectrum_input: SpectrumInput,
    stereo_spectrum_output: Arc<Mutex<SpectrumOutput>>,

    bypass_smoother: Arc<Smoother<f32>>,
    mix_delays: Vec<delayline::DelayLine>
}

pub struct ScratchBuffers {
    drive: [f32; MAX_OVERSAMPLED_BLOCK_SIZE],
    curve: [f32; MAX_OVERSAMPLED_BLOCK_SIZE],
    blend: [f32; MAX_OVERSAMPLED_BLOCK_SIZE],
    high_pass_freq: [f32; MAX_OVERSAMPLED_BLOCK_SIZE],
    peak_freq: [f32; MAX_OVERSAMPLED_BLOCK_SIZE],
    peak_gain: [f32; MAX_OVERSAMPLED_BLOCK_SIZE],
    low_pass_freq: [f32; MAX_OVERSAMPLED_BLOCK_SIZE],
    bypass: [f32; MAX_BLOCK_SIZE]
}

impl Default for ScratchBuffers {
    fn default() -> Self {
        Self {
            drive: [0.0; MAX_OVERSAMPLED_BLOCK_SIZE],
            curve: [0.0; MAX_OVERSAMPLED_BLOCK_SIZE],
            blend: [0.0; MAX_OVERSAMPLED_BLOCK_SIZE],
            high_pass_freq: [0.0; MAX_OVERSAMPLED_BLOCK_SIZE],
            peak_freq: [0.0; MAX_OVERSAMPLED_BLOCK_SIZE],
            peak_gain: [0.0; MAX_OVERSAMPLED_BLOCK_SIZE],
            low_pass_freq: [0.0; MAX_OVERSAMPLED_BLOCK_SIZE],
            bypass: [0.0; MAX_BLOCK_SIZE],
        }
    }
}

#[derive(Params)]
struct NebulaParams {
    #[persist = "editor-state"]
    editor_state: Arc<EguiState>,

    #[id = "drive"]
    pub drive: FloatParam,

    #[id = "curve"]
    pub curve: FloatParam,

    #[id = "blend"]
    pub blend: FloatParam,

    #[id = "high_pass_freq"]
    pub high_pass_freq: FloatParam,

    #[id = "peak_freq"]
    pub peak_freq: FloatParam,

    #[id = "peak_gain"]
    pub peak_gain: FloatParam,

    #[id = "low_pass_freq"]
    pub low_pass_freq: FloatParam,

    #[id = "bypass"]
    pub bypass: BoolParam,

    #[id = "hysteresis"]
    pub hysteresis: BoolParam,
}

impl Default for Nebula {
    fn default() -> Self {
        let sample_rate = Arc::new(AtomicF32::new(1.0));
        let num_channels = Arc::new(AtomicUsize::new(1));
        let bypass_smoother = Arc::new(Smoother::new(SmoothingStyle::Linear(10.0)));
        let (mono_spectrum_input, mono_spectrum_output) = SpectrumInput::new(1 as usize);
        let (stereo_spectrum_input, stereo_spectrum_output) = SpectrumInput::new(2 as usize);

        Self {
            params: Arc::new(NebulaParams::new(sample_rate.clone(), bypass_smoother.clone())),
            sample_rate,
            num_channels,
            buffers: Box::default(),
            oversamplers: Vec::new(),
            input_copies: Vec::new(),
            hysteresises: Vec::new(),
            saturation_effects: Vec::new(),
            high_pass_filters: Vec::new(),
            high_pass_coefficients: biquad::BiquadCoefficients::identity(),
            prev_high_pass_freq: 0.0,
            peak_filters: Vec::new(),
            peak_coefficients: biquad::BiquadCoefficients::identity(),
            prev_peak_freq: 0.0,
            prev_peak_gain: 0.0,
            low_pass_filters: Vec::new(),
            low_pass_coefficients: biquad::BiquadCoefficients::identity(),
            prev_low_pass_freq: 0.0,
            dc_filters: Vec::new(),
            dc_filter_coefficients: biquad::BiquadCoefficients::identity(),
            mono_spectrum_input,
            mono_spectrum_output: Arc::new(Mutex::new(mono_spectrum_output)),
            stereo_spectrum_input,
            stereo_spectrum_output: Arc::new(Mutex::new(stereo_spectrum_output)),
            bypass_smoother,
            mix_delays: Vec::new()
        }
    }
}

impl NebulaParams {
    pub fn new(sample_rate: Arc<AtomicF32>, bypass_smoother: Arc<Smoother<f32>>) -> Self {
        Self {
            editor_state: EguiState::from_size(620, 480),

            drive: FloatParam::new("Drive", util::db_to_gain(0.0),
                FloatRange::Skewed {
                    min: util::db_to_gain(0.0),
                    max: util::db_to_gain(20.0),
                    factor: FloatRange::gain_skew_factor(0.0, 20.0),
                },
            )
            .with_smoother(SmoothingStyle::Logarithmic(50.0 * OVERSAMPLING_TIMES as f32))
            .with_unit(" dB")
            .with_value_to_string(formatters::v2s_f32_gain_to_db(1))
            .with_string_to_value(formatters::s2v_f32_gain_to_db()),

            curve: FloatParam::new("Curve", 0.5, FloatRange::Linear { min: 0.0, max: 1.0 })
            // Using linear smoothing because of fixed oversample. If an oversample parameter
            // is introduced, then this should use SmoothingStyle::OversamplingAware instead.
            .with_smoother(SmoothingStyle::Linear(20.0 * OVERSAMPLING_TIMES as f32))
            .with_unit("%")
            .with_value_to_string(formatters::v2s_f32_percentage(0))
            .with_string_to_value(formatters::s2v_f32_percentage()),

            blend: FloatParam::new("Blend", 0.8, FloatRange::Linear { min: 0.0, max: 1.0 })
            // Using linear smoothing because of fixed oversample. If an oversample parameter
            // is introduced, then this should use SmoothingStyle::OversamplingAware instead.
            .with_smoother(SmoothingStyle::Linear(20.0 * OVERSAMPLING_TIMES as f32))
            .with_unit("%")
            .with_value_to_string(formatters::v2s_f32_percentage(0))
            .with_string_to_value(formatters::s2v_f32_percentage()),

            high_pass_freq: FloatParam::new("High Pass", MIN_FILTER_FREQUENCY,
            FloatRange::Skewed { min: MIN_FILTER_FREQUENCY, max: MAX_FILTER_FREQUENCY, factor: FloatRange::skew_factor(-1.0) })
            .with_smoother(SmoothingStyle::Logarithmic(50.0 * OVERSAMPLING_TIMES as f32))
            .with_value_to_string(Arc::new(|value| { format!("{value:.0} Hz") }))
            .with_string_to_value(Arc::new(|string| { string.trim().trim_end_matches(" Hz").parse().ok() })),

            peak_freq: FloatParam::new("Peak Freq", 1000.0,
            FloatRange::Skewed { min: MIN_FILTER_FREQUENCY, max: MAX_FILTER_FREQUENCY, factor: FloatRange::skew_factor(-1.0) },
            )
            .with_smoother(SmoothingStyle::Logarithmic(50.0 * OVERSAMPLING_TIMES as f32))
            .with_value_to_string(Arc::new(|value| { format!("{value:.0} Hz") }))
            .with_string_to_value(Arc::new(|string| { string.trim().trim_end_matches(" Hz").parse().ok() })),

            peak_gain: FloatParam::new("Peak Gain", 0.0, FloatRange::Linear { min: MIN_FILTER_GAIN, max: MAX_FILTER_GAIN })
            .with_smoother(SmoothingStyle::Linear(50.0 * OVERSAMPLING_TIMES as f32))
            .with_unit(" dB")
            .with_value_to_string(Arc::new(|value| { format!("{value:.0} dB") }))
            .with_string_to_value(Arc::new(|string| { string.trim().trim_end_matches(" dB").parse().ok() })),

            low_pass_freq: FloatParam::new("Low Pass Freq", MAX_FILTER_FREQUENCY,
            FloatRange::Skewed { min: MIN_FILTER_FREQUENCY, max: MAX_FILTER_FREQUENCY, factor: FloatRange::skew_factor(-1.0) })
            .with_smoother(SmoothingStyle::Logarithmic(50.0 * OVERSAMPLING_TIMES as f32))
            .with_value_to_string(Arc::new(|value| { format!("{value:.0} Hz") }))
            .with_string_to_value(Arc::new(|string| { string.trim().trim_end_matches(" Hz").parse().ok() })),

            bypass: BoolParam::new("Bypass", false)
            .with_callback(Arc::new(move |value| {
                bypass_smoother.set_target(
                    sample_rate.load(Ordering::Relaxed),
                    if value { 1.0 } else { 0.0 },
                );
            }))
            .with_value_to_string(formatters::v2s_bool_bypass())
            .with_string_to_value(formatters::s2v_bool_bypass())
            .make_bypass(),

            hysteresis: BoolParam::new("Hysteresis", true)
            .with_value_to_string(formatters::v2s_bool_bypass())
            .with_string_to_value(formatters::s2v_bool_bypass())
        }
    }
}

impl Plugin for Nebula {
    const NAME: &'static str = "Nebula (saturation)";
    const VENDOR: &'static str = "Starlight Audio";
    const URL: &'static str = "https://starlightaudio.xyz";
    const EMAIL: &'static str = "info@starlightaudio.xyz";

    const VERSION: &'static str = env!("CARGO_PKG_VERSION");

    const AUDIO_IO_LAYOUTS: &'static [AudioIOLayout] = &[
        AudioIOLayout {
            main_input_channels: NonZeroU32::new(2),
            main_output_channels: NonZeroU32::new(2),

            aux_input_ports: &[],
            aux_output_ports: &[],

            names: PortNames::const_default(),
        },
        AudioIOLayout {
            main_input_channels: NonZeroU32::new(1),
            main_output_channels: NonZeroU32::new(1),
            ..AudioIOLayout::const_default()
        },
    ];

    const MIDI_INPUT: MidiConfig = MidiConfig::None;
    const SAMPLE_ACCURATE_AUTOMATION: bool = true;

    type SysExMessage = ();
    type BackgroundTask = ();

    fn params(&self) -> Arc<dyn Params> {
        self.params.clone()
    }

    fn initialize(
        &mut self,
        audio_io_layout: &AudioIOLayout,
        buffer_config: &BufferConfig,
        context: &mut impl InitContext<Self>,
    ) -> bool {
        let num_channels = audio_io_layout
            .main_output_channels
            .expect("Plugin was initialized without any outputs")
            .get() as usize;

        self.num_channels.store(num_channels, Ordering::Relaxed);
        self.sample_rate.store(buffer_config.sample_rate, Ordering::Relaxed);

        self.oversamplers.resize_with(num_channels, || {
            oversampling::Lanczos3Oversampler::new(MAX_BLOCK_SIZE, OVERSAMPLING_FACTOR)
        });

        if let Some(oversampler) = self.oversamplers.first() {
            context.set_latency_samples(oversampler.latency(OVERSAMPLING_FACTOR));

            self.mix_delays.resize_with(num_channels, || {
                delayline::DelayLine::new(oversampler.latency(OVERSAMPLING_FACTOR))
            });
        }

        self.input_copies.resize_with(num_channels, || {
            [0.0; MAX_BLOCK_SIZE]
        });

        self.hysteresises.resize_with(num_channels, || {
            Hysteresis::new()
        });

        self.saturation_effects.resize_with(num_channels, || {
            Saturation::new()
        });

        self.high_pass_filters.resize_with(num_channels, || { biquad::Biquad::new() });
        self.high_pass_coefficients = biquad::BiquadCoefficients::high_pass(
            buffer_config.sample_rate * OVERSAMPLING_TIMES as f32,
                self.params.high_pass_freq.value(), DEFAULT_Q);

        self.peak_filters.resize_with(num_channels, || { biquad::Biquad::new() });
        self.peak_coefficients = biquad::BiquadCoefficients::peak(
            buffer_config.sample_rate * OVERSAMPLING_TIMES as f32,
            self.params.peak_freq.value(), DEFAULT_Q,
            self.params.peak_gain.value());

        self.low_pass_filters.resize_with(num_channels, || { biquad::Biquad::new() });
        self.low_pass_coefficients = biquad::BiquadCoefficients::low_pass(
            buffer_config.sample_rate * OVERSAMPLING_TIMES as f32,
            self.params.low_pass_freq.value(), DEFAULT_Q);

        self.dc_filters.resize_with(num_channels, || { biquad::Biquad::new() });
        self.dc_filter_coefficients = biquad::BiquadCoefficients::high_pass(
            buffer_config.sample_rate, 20.0, DEFAULT_Q);

        self.mono_spectrum_input.update_sample_rate(buffer_config.sample_rate);
        self.stereo_spectrum_input.update_sample_rate(buffer_config.sample_rate);

        true
    }

    fn reset(&mut self) {
        for oversampler in &mut self.oversamplers {
            oversampler.reset();
        }

        for hysteresis in &mut self.hysteresises {
            hysteresis.reset();
        }

        self.bypass_smoother.reset(if self.params.bypass.value() { 1.0 } else { 0.0 });
    }

    fn process(
        &mut self,
        buffer: &mut Buffer,
        _aux: &mut AuxiliaryBuffers,
        _context: &mut impl ProcessContext<Self>,
    ) -> ProcessStatus {
        for (_, mut block) in buffer.iter_blocks(MAX_BLOCK_SIZE) {
            let block_len = block.samples();
            let upsampled_block_len = block_len * OVERSAMPLING_TIMES;
            let sample_rate = self.sample_rate.load(Ordering::Relaxed);

            // These parameters are smoothed at the oversample rate
            self.params.drive.smoothed.next_block(&mut self.buffers.drive, upsampled_block_len);
            self.params.curve.smoothed.next_block(&mut self.buffers.curve, upsampled_block_len);
            self.params.blend.smoothed.next_block(&mut self.buffers.blend, upsampled_block_len);
            self.params.high_pass_freq.smoothed.next_block(&mut self.buffers.high_pass_freq, upsampled_block_len);
            self.params.peak_freq.smoothed.next_block(&mut self.buffers.peak_freq, upsampled_block_len);
            self.params.peak_gain.smoothed.next_block(&mut self.buffers.peak_gain, upsampled_block_len);
            self.params.low_pass_freq.smoothed.next_block(&mut self.buffers.low_pass_freq, upsampled_block_len);

            // This parameter is smoothed at the original sample rate
            self.bypass_smoother.next_block(&mut self.buffers.bypass, block_len);

            for (block_channel, input_copy) in block.iter_mut().zip(self.input_copies.iter_mut()) {
                input_copy[..block_len].copy_from_slice(block_channel);
            }

            for ((((((block_channel,
                    hysteresis),
                    oversampler),
                    saturation_effect),
                    hpf),
                    peak),
                    lpf) in block.iter_mut()
                    .zip(self.hysteresises.iter_mut())
                    .zip(self.oversamplers.iter_mut())
                    .zip(self.saturation_effects.iter_mut())
                    .zip(self.high_pass_filters.iter_mut()) 
                    .zip(self.peak_filters.iter_mut()) 
                    .zip(self.low_pass_filters.iter_mut())  {

                oversampler.process(block_channel, OVERSAMPLING_FACTOR, |upsampled| {
                    for (((((((sample, drive), curve), blend), high_pass_freq), peak_freq), peak_gain), low_pass_freq) in
                        upsampled.iter_mut()
                            .zip(self.buffers.drive)
                            .zip(self.buffers.curve)
                            .zip(self.buffers.blend)
                            .zip(self.buffers.high_pass_freq)
                            .zip(self.buffers.peak_freq)
                            .zip(self.buffers.peak_gain)
                            .zip(self.buffers.low_pass_freq) {

                        // Avoid computing biquad coefficients unless the parameters have changed
                        if self.prev_high_pass_freq != high_pass_freq {
                            self.high_pass_coefficients = biquad::BiquadCoefficients::high_pass(
                                sample_rate * OVERSAMPLING_TIMES as f32, high_pass_freq, DEFAULT_Q);
                            self.prev_high_pass_freq = high_pass_freq;
                        }

                        if self.prev_peak_freq != peak_freq || self.prev_peak_gain != peak_gain {
                            self.peak_coefficients = biquad::BiquadCoefficients::peak(
                                sample_rate * OVERSAMPLING_TIMES as f32,
                                peak_freq, DEFAULT_Q, peak_gain);
                            self.prev_peak_freq = peak_freq;
                            self.prev_peak_gain = peak_gain;
                        }

                        if self.prev_low_pass_freq != low_pass_freq {
                            self.low_pass_coefficients = biquad::BiquadCoefficients::low_pass(
                                sample_rate * OVERSAMPLING_TIMES as f32,
                                low_pass_freq, DEFAULT_Q);
                            self.prev_low_pass_freq = low_pass_freq;
                        }

                        let mut gain = saturation_effect.process(*sample * drive, blend, curve);

                        gain -= 1.0;
                        gain = hpf.process(gain, &self.high_pass_coefficients);
                        gain = peak.process(gain, &self.peak_coefficients);
                        gain = lpf.process(gain, &self.low_pass_coefficients);
                        gain += 1.0;
                        
                        if self.params.hysteresis.value() {
                            gain = hysteresis.process(gain, sample_rate * OVERSAMPLING_TIMES as f32);
                        }
                        *sample *= gain;

                    }
                });
            }

            for (((block_channel, dc_filter), input_copy), mix_delay) in
                block.iter_mut().zip(self.dc_filters.iter_mut()).zip(self.input_copies.iter()).zip(self.mix_delays.iter_mut()) {

                for ((sample, input_sample), bypass) in
                    block_channel.iter_mut().zip(input_copy).zip(self.buffers.bypass) {

                    // Delayed input to match the latency from the oversampler.
                    let dry = mix_delay.next_sample(*input_sample);
                    let wet = dc_filter.process(*sample, &self.dc_filter_coefficients);
                    *sample = emath::lerp(0.0..=1.0, bypass) * dry +
                              emath::lerp(1.0..=0.0, bypass) * wet;
                }
            }
        }

        if self.params.editor_state.is_open() {
            match buffer.channels() {
                1 => self.mono_spectrum_input.compute(buffer),
                2 => self.stereo_spectrum_input.compute(buffer),
                _ => ()
            }
        }

        ProcessStatus::Normal
    }

    fn editor(&mut self, _async_executor: AsyncExecutor<Self>) -> Option<Box<dyn Editor>> {
        let params = self.params.clone();
        let sample_rate = self.sample_rate.clone();
        let num_channels = self.num_channels.clone();
        let mono_spectrum = self.mono_spectrum_output.clone();
        let stereo_spectrum = self.stereo_spectrum_output.clone();

        create_egui_editor(
            self.params.editor_state.clone(),
            (),
            |_, _| {},
            move |egui_ctx, setter, _state| {

                let tc = ToneControl::new(
                    &params.high_pass_freq,
                    &params.peak_freq,
                    &params.peak_gain,
                    &params.low_pass_freq,
                    setter,
                    &sample_rate,
                    &num_channels,
                    &mono_spectrum,
                    &stereo_spectrum,
                    580.0, 210.0
                );

                egui::CentralPanel::default().show(egui_ctx, |ui| {
                    ui.spacing_mut().item_spacing = Vec2::new(0.0, 0.0);

                    ui.horizontal(|ui| {
                        ui.add_space(13.0);  // Plugin window has a 7px margin. Add 13px to make it 20px
                        ui.vertical(|ui| {
                            ui.add_space(13.0);  // Plugin window has a 7px margin. Add 13px to make it 20px
                            ui.horizontal(|ui| {
                                ui.add(Dial::for_param(&params.drive, setter));
                                ui.vertical(|ui| {
                                    ui.add_space(10.0);
                                    ui.horizontal(|ui| {
                                        ui.add_space(30.0);
                                        ui.add(ResponseGraph::new(      
                                            &params.drive,
                                            &params.curve,
                                            &params.blend,
                                            160.0, 160.0));
                                    });
                                    ui.add_space(18.0);
                                    ui.add(Slider::for_param(&params.blend, setter, "TAPE", "TUBE", 220.0));
                                });
                                ui.add(Dial::for_param(&params.curve, setter));
                            });

                            ui.add_space(20.0);
                            ui.add(tc);
                        });
                    });

                });
            },
        )
    }

}


impl ClapPlugin for Nebula {
    const CLAP_ID: &'static str = "xyz.starlightaudio.nebula";
    const CLAP_DESCRIPTION: Option<&'static str> = Some("Saturation");
    const CLAP_MANUAL_URL: Option<&'static str> = Some(Self::URL);
    const CLAP_SUPPORT_URL: Option<&'static str> = None;
    const CLAP_FEATURES: &'static [ClapFeature] = &[
        ClapFeature::AudioEffect,
        ClapFeature::Stereo,
        ClapFeature::Mono,
        ClapFeature::Distortion,
    ];
}

impl Vst3Plugin for Nebula {
    const VST3_CLASS_ID: [u8; 16] = *b"Starlight_Nebula";
    const VST3_SUBCATEGORIES: &'static [Vst3SubCategory] = &[Vst3SubCategory::Fx, Vst3SubCategory::Distortion];
}

nih_export_clap!(Nebula);
nih_export_vst3!(Nebula);
