use std::f32::consts::{E, LN_10, PI};
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::{Arc, Mutex};

use atomic_float::AtomicF32;
use lazy_static::lazy_static;

use nih_plug::params::FloatParam;
use nih_plug::prelude::{Param, ParamSetter};

use nih_plug::util;
use nih_plug_egui::egui::epaint::Hsva;
use nih_plug_egui::egui::{self, pos2, Align2, Color32, FontId, Pos2, Rangef, Rect};
use nih_plug_egui::egui::{ Response, Sense, Stroke, Ui, Vec2, Widget };

use starlight_audio_dsp::biquad::{BiquadCoefficients, DEFAULT_Q};
use starlight_audio_dsp::spectrum::SpectrumOutput;

use crate::lerp_color;

const MIN_FREQ: f32 = 20.0;
const MAX_FREQ: f32 = 20000.0;

const MAX_DB: f32 = 6.0;
const MIN_DB: f32 = -3.0;

const CIRCLE_RADIUS: f32 = 8.0;
const CIRCLE_RADIUS_SQUARED: f32 = CIRCLE_RADIUS * CIRCLE_RADIUS;

const MARGIN_LEFT: f32 = 50.0;
const MARGIN_RIGHT: f32 = 50.0;
const MARGIN_TOP: f32 = 35.0;
const MARGIN_BOTTOM: f32 = 20.0;


lazy_static! {
    static ref DRAGGED_CONTROL_MEMORY_ID: egui::Id = egui::Id::new((file!(), 0));
}

#[derive(Clone)]
enum DraggedControl {
    None,
    HighPass,
    Peak,
    LowPass,
}

pub struct ToneControl<'a> {
    pub high_pass_freq_param:  &'a FloatParam,
    pub peak_freq_param: &'a FloatParam,
    pub peak_gain_param: &'a FloatParam,
    pub low_pass_freq_param: &'a FloatParam,
    pub param_setter: &'a ParamSetter<'a>,
    pub sample_rate: &'a Arc<AtomicF32>,
    pub num_channels: &'a Arc<AtomicUsize>,
    pub mono_spectrum: &'a Arc<Mutex<SpectrumOutput>>,
    pub stereo_spectrum: &'a Arc<Mutex<SpectrumOutput>>,

    low_color: Hsva,
    high_color: Hsva,

    // pixel dimensions
    pub width: f32,
    pub height: f32
}

impl<'a> ToneControl<'a> {

    pub fn new(
        high_pass_freq_param:  &'a FloatParam,
        peak_freq_param: &'a FloatParam,
        peak_gain_param: &'a FloatParam,
        low_pass_freq_param: &'a FloatParam,
        param_setter: &'a ParamSetter<'a>,
        sample_rate: &'a Arc<AtomicF32>,
        num_channels: &'a Arc<AtomicUsize>,
        mono_spectrum: &'a Arc<Mutex<SpectrumOutput>>,
        stereo_spectrum: &'a Arc<Mutex<SpectrumOutput>>,
        width: f32,
        height: f32) -> Self {

        let low_color = Hsva::new(0.00, 0.90, 0.72, 1.0);
        let high_color = Hsva::new(0.15, 0.90, 0.95, 1.0);

        Self {
            high_pass_freq_param,
            peak_freq_param,
            peak_gain_param,
            low_pass_freq_param,
            param_setter,
            sample_rate,
            num_channels,
            mono_spectrum,
            stereo_spectrum,
            low_color,
            high_color,
            width,
            height
        }
    }

    // x is relative to the left of the rect, not absolute ui position!
    fn freq_to_x(&self, rect: &Rect, freq: f32) -> f32 {
        rect.width() * (freq / MIN_FREQ).ln() / (MAX_FREQ / MIN_FREQ).ln()
    }

    // x is relative to the rect, not absolute ui position!
    fn x_to_freq(&self, rect: &Rect, x: f32) -> f32 {
        E.powf(x * (MAX_FREQ / MIN_FREQ).ln() / rect.width())  *  MIN_FREQ
    }

    fn y_to_gain(&self, rect: &Rect, y: f32) -> f32 {
        egui::emath::remap_clamp(y / rect.height(), -0.5..=0.5, Rangef::new(MAX_DB, MIN_DB))
    }

    // This is absolute position! FIX - decide on whether absolute or relative makes more sense
    fn gain_to_y(&self, rect: &Rect, gain: f32) -> f32 {
        rect.center().y + rect.height() * egui::emath::remap_clamp(
            gain, Rangef::new(MAX_DB, MIN_DB), -0.5..=0.5)
    }

    fn get_high_pass_control_position(&self, rect: &Rect) -> Pos2 {
        Pos2 {
            x: rect.left() + self.freq_to_x(rect, self.high_pass_freq_param.modulated_plain_value()),
            y: egui::emath::remap_clamp(-3.0, Rangef::new(MAX_DB, MIN_DB), rect.y_range())
        }
    }

    fn get_peak_control_position(&self, rect: &Rect) -> Pos2 {
        Pos2 {
            x: rect.left() + self.freq_to_x(rect, self.peak_freq_param.modulated_plain_value()),
            y: egui::emath::remap_clamp(
                self.peak_gain_param.modulated_plain_value(),
                Rangef::new(MAX_DB, MIN_DB),
                rect.y_range())
        }
    }

    fn get_low_pass_control_position(&self, rect: &Rect) -> Pos2 {
        Pos2 {
            x: rect.left() + self.freq_to_x(rect, self.low_pass_freq_param.modulated_plain_value()),
            y: egui::emath::remap_clamp(-3.0, Rangef::new(MAX_DB, MIN_DB), rect.y_range())
        }
    }

    fn set_value(&self, param: &FloatParam, value: f32) {
        if value != param.modulated_plain_value() {
            self.param_setter.set_parameter(param, value);
        }
    }

    // DANGER! With a constant memory id, this will only work for a single widget
    // Need to figure out how to get a unique id per ToneControl widget!
    fn get_dragged_control(&self, ui: &Ui) -> DraggedControl {
        ui.memory(|mem| {
            mem.data
                .get_temp(*DRAGGED_CONTROL_MEMORY_ID)
                .unwrap_or(DraggedControl::None)
        })
    }

    fn set_dragged_control(&self, ui: &Ui, dragged_control: DraggedControl) {
        ui.memory_mut(|mem| {
            mem.data.insert_temp(*DRAGGED_CONTROL_MEMORY_ID, dragged_control)
        });
    }

    fn tone_control_ui(&mut self, ui: &mut Ui, response: &mut Response) {
        let eq_rect = Rect::from_two_pos(
            response.rect.left_top() + Vec2::new(MARGIN_LEFT, MARGIN_TOP),
            response.rect.right_bottom() - Vec2::new(MARGIN_RIGHT, MARGIN_BOTTOM),
        );

        if response.drag_started() {
            if let Some(pointer_pos) = response.interact_pointer_pos() {
                let drag_start_pos = pointer_pos - response.drag_delta();
                if self.get_low_pass_control_position(&eq_rect).distance_sq(drag_start_pos) <= CIRCLE_RADIUS_SQUARED {
                    self.set_dragged_control(&ui, DraggedControl::LowPass);
                    self.param_setter.begin_set_parameter(self.low_pass_freq_param);
                }
                else if self.get_peak_control_position(&eq_rect).distance_sq(drag_start_pos) <= CIRCLE_RADIUS_SQUARED {
                    self.set_dragged_control(&ui, DraggedControl::Peak);
                    self.param_setter.begin_set_parameter(self.peak_freq_param);
                    self.param_setter.begin_set_parameter(self.peak_gain_param);
                }
                else if self.get_high_pass_control_position(&eq_rect).distance_sq(drag_start_pos) <= CIRCLE_RADIUS_SQUARED {
                    self.set_dragged_control(&ui, DraggedControl::HighPass);
                    self.param_setter.begin_set_parameter(self.high_pass_freq_param);
                }
            }
        }

        let drag_delta = response.drag_delta();
        if drag_delta != Vec2::ZERO {
            match self.get_dragged_control(ui) {
                DraggedControl::HighPass => {
                    let pos = self.get_high_pass_control_position(&eq_rect) + drag_delta;
                    let freq = self.x_to_freq(&eq_rect, pos.x - eq_rect.left());
                    self.set_value(self.high_pass_freq_param, freq);
                    response.mark_changed();
                }
                DraggedControl::Peak => {
                    let pos = self.get_peak_control_position(&eq_rect) + drag_delta;
                    let freq = self.x_to_freq(&eq_rect, pos.x - eq_rect.left());
                    let gain_db = self.y_to_gain(&eq_rect, pos.y - eq_rect.center().y);
                    self.set_value(self.peak_freq_param, freq);
                    self.set_value(self.peak_gain_param, gain_db);
                    response.mark_changed();
                }
                DraggedControl::LowPass => {
                    let pos = self.get_low_pass_control_position(&eq_rect) + drag_delta;
                    let freq = self.x_to_freq(&eq_rect, pos.x - eq_rect.left());
                    self.set_value(self.low_pass_freq_param, freq);
                    response.mark_changed();
                }
                DraggedControl::None => { }
            }
        }

        if response.drag_stopped() {
            match self.get_dragged_control(ui) {
                DraggedControl::HighPass => {
                    self.param_setter.end_set_parameter(self.high_pass_freq_param);
                }
                DraggedControl::Peak => {
                    self.param_setter.end_set_parameter(self.peak_freq_param);
                    self.param_setter.end_set_parameter(self.peak_gain_param);
                }
                DraggedControl::LowPass => {
                    self.param_setter.end_set_parameter(self.low_pass_freq_param);
                }
                DraggedControl::None => { }
            }

            self.set_dragged_control(ui, DraggedControl::None);
        }

        if ui.is_rect_visible(response.rect) {

            ui.painter().rect_filled(response.rect, 10.0, Color32::BLACK);

            // TODO: Better color management!
            let guideline_color = Color32::from(Hsva::new(0.00, 0.90, 0.10, 1.0));
            let spectrum_color = Color32::from(Hsva::new(0.00, 0.90, 0.05, 1.0));

            // Draw spectrum analysis
            let spectrum = match self.num_channels.load(Ordering::Relaxed) {
                1 => Some(self.mono_spectrum),
                2 => Some(self.stereo_spectrum),
                _ => None // Won't happen unless plugin is modified to support more than two channels
            };

            match spectrum {
                None => (),
                Some(s) => {

                    let mut spectrum = s.lock().unwrap();
                    let buffer = spectrum.read();
                    let nyquist = self.sample_rate.load(Ordering::Relaxed) / 2.0;

                    if nyquist > 0.0 {  // 0.0 means that DSP isn't initialized yet, skip rendering this
                        for x in 0..(eq_rect.width() as i32) {
                            let freq = self.x_to_freq(&eq_rect, x as f32);
                            let bin_idx = buffer.len() as f32 * freq / nyquist;
                            let decibels = emath::lerp(
                                util::gain_to_db_fast(buffer[bin_idx.floor() as usize]) ..=
                                util::gain_to_db_fast(buffer[bin_idx.ceil() as usize]),
                                bin_idx - bin_idx.floor(),
                            );

                            let top = egui::emath::remap_clamp(decibels,
                                Rangef::new(0.0, -80.0),
                                eq_rect.y_range());

                            ui.painter().vline(eq_rect.left() + x as f32,
                                Rangef::new(top, eq_rect.max.y),
                                Stroke::new(1.0, spectrum_color));
                        }
                    }
                }
            }

            // Draw frequency guidelines and labels
            for freq in [
                20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900,
                1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 20000] {
                    let x = eq_rect.left() + self.freq_to_x(&eq_rect, freq as f32);
                    ui.painter().vline(x, eq_rect.y_range(), Stroke::new(1.0, guideline_color));
            }

            for freq in [20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000] {
                let x = eq_rect.left() + self.freq_to_x(&eq_rect, freq as f32);
                let text = if freq < 1000 {
                    format!("{}Hz", freq as i32)
                } else {
                    format!("{}kHz", freq as i32 / 1000)
                };
                ui.painter().text(Pos2::new(x, eq_rect.top() - 10.0), Align2::CENTER_BOTTOM, text, FontId::default(), Color32::GRAY);
                let x = eq_rect.left() + self.freq_to_x(&eq_rect, freq as f32);
                ui.painter().vline(x, eq_rect.y_range(), Stroke::new(2.0, guideline_color));
            }

            // Draw gain guidelines and labels
            for gain in [-3.0, 0.0, 3.0, 6.0] {
                let y = self.gain_to_y(&eq_rect, gain);
                ui.painter().hline(eq_rect.x_range(), y, Stroke::new(2.0, guideline_color));
                ui.painter().text(Pos2::new(&eq_rect.left() - 10.0, y), Align2::RIGHT_CENTER, format!("{} dB", gain as i32), FontId::default(), Color32::GRAY);
                ui.painter().text(Pos2::new(&eq_rect.right() + 10.0, y), Align2::LEFT_CENTER, format!("{} dB", gain as i32), FontId::default(), Color32::GRAY);
            }

            let clipped = ui.painter().with_clip_rect(eq_rect);

            for i in 0..eq_rect.width() as i32 {
                let freq = self.x_to_freq(&eq_rect, i as f32);
                let w = freq / (2.0 * MAX_FREQ) * PI;
                let phi = (w / 2.0).sin() * (w / 2.0).sin();

                // TODO: See if this can come from the DSP code... somehow...
                let high_pass_coefficients = BiquadCoefficients::high_pass(MAX_FREQ * 4.0,
                    self.high_pass_freq_param.modulated_plain_value(), DEFAULT_Q);

                let peak_coefficients = BiquadCoefficients::peak(MAX_FREQ * 4.0,
                    self.peak_freq_param.modulated_plain_value(), DEFAULT_Q,
                    self.peak_gain_param.modulated_plain_value());

                let low_pass_coefficients = BiquadCoefficients::low_pass(MAX_FREQ * 4.0,
                    self.low_pass_freq_param.modulated_plain_value(), DEFAULT_Q);

                let amplitude = (
                    high_pass_coefficients.freq_response(phi) +
                    peak_coefficients.freq_response(phi) +
                    low_pass_coefficients.freq_response(phi)
                ) * 10.0 / LN_10;

                let y = egui::emath::remap(amplitude,
                    Rangef::new(MAX_DB, MIN_DB),
                    eq_rect.y_range());

                let value = egui::emath::remap_clamp(amplitude, Rangef::new(0.0, MAX_DB), 0.0..=1.0);
                let color = lerp_color(self.low_color, self.high_color, value);
                clipped.circle_stroke(pos2(eq_rect.left() + i as f32, y), 1.0, Stroke::new(1.0, color));
            }

            let value = egui::emath::remap_clamp(self.peak_gain_param.modulated_plain_value(), Rangef::new(0.0, MAX_DB), 0.0..=1.0);
            let color = lerp_color(self.low_color, self.high_color, value);
            ui.painter().circle_filled(self.get_peak_control_position(&eq_rect), CIRCLE_RADIUS, color);

            ui.painter().circle_filled(self.get_high_pass_control_position(&eq_rect), CIRCLE_RADIUS, self.low_color);
            ui.painter().circle_filled(self.get_low_pass_control_position(&eq_rect), CIRCLE_RADIUS, self.low_color);
        }
    }

}

impl Widget for ToneControl<'_> {
    fn ui(mut self, ui: &mut Ui) -> Response {
        let desired_size = Vec2::new(self.width, self.height);
        let mut response = ui.allocate_response(desired_size, Sense::click_and_drag());
        self.tone_control_ui(ui, &mut response);
        response
    }
}