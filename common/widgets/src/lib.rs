use nih_plug_egui::egui::{epaint::Hsva, Color32};

pub mod dial;
pub mod tone_control;
pub mod slider;
pub mod response_graph;


#[inline]
fn lerp_color(low: Hsva, high: Hsva, x: f32) -> Color32 {
    Color32::from(Hsva::new(
        emath::lerp(low.h..=high.h, x),
        emath::lerp(low.s..=high.s, x),
        emath::lerp(low.v..=high.v, x),
        emath::lerp(low.a..=high.a, x)))
}
