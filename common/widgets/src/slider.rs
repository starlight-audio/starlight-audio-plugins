use nih_plug::params::FloatParam;
use nih_plug::prelude::{Param, ParamSetter};

use nih_plug_egui::egui::epaint::Hsva;
use nih_plug_egui::egui::{Align2, Event, FontId, Vec2};
use nih_plug_egui::egui::Color32;
use nih_plug_egui::egui::{ emath, vec2, Response, Sense, Ui, Widget };

use crate::lerp_color;


//
//  TODO: Lots of possibilities
//  * Make it support both vertical and horizontal sliders
//  * Make an option to fills in the stripe, instead of moving a dot
pub struct Slider<'a> {
    param: &'a FloatParam,
    setter: &'a ParamSetter<'a>,
    outer_length: f32,
    inner_length: f32,
    stripe: f32,    // half the width of the stripe
    handle: f32,    // half the width of the handle
    low_color: Hsva,
    high_color: Hsva,
    low_label: String,
    high_label: String
}

impl<'a> Slider<'a> {
    pub fn for_param(param: &'a FloatParam, setter: &'a ParamSetter<'a>, low_label: impl Into<String>, high_label: impl Into<String>, width: f32) -> Self {
        let outer_length = width;
        let inner_length = outer_length - 100.0;
        let stripe = 2.0;
        let handle = 8.0;
        let low_color = Hsva::new(0.00, 0.90, 0.72, 1.0);
        let high_color = Hsva::new(0.15, 0.90, 0.95, 1.0);

        Self {
            param, setter, outer_length, inner_length, stripe, handle, low_color, high_color,
            low_label: low_label.into(), high_label: high_label.into()
        }
    }

    fn plain_value(&self) -> f32 {
        self.param.modulated_plain_value()
    }

    fn normalized_value(&self) -> f32 {
        self.param.modulated_normalized_value()
    }

    fn begin_drag(&self) {
        self.setter.begin_set_parameter(self.param);
    }

    fn set_normalized_value(&self, normalized: f32) {
        // This snaps to the nearest plain value if the parameter is stepped in some way.
        // TODO: As an optimization, we could add a `const CONTINUOUS: bool` to the parameter to
        //       avoid this normalized->plain->normalized conversion for parameters that don't need
        //       it
        let value = self.param.preview_plain(normalized);
        if value != self.plain_value() {
            self.setter.set_parameter(self.param, value);
        }
    }

    /// Begin and end drag still need to be called when using this..
    fn reset_param(&self) {
        self.setter.set_parameter(self.param, self.param.default_plain_value());
    }

    fn end_drag(&self) {
        self.setter.end_set_parameter(self.param);
    }

    fn slider_ui(&self, ui: &Ui, response: &mut Response) {
        if response.drag_started() {
            self.begin_drag();
        }
        if let Some(click_pos) = response.interact_pointer_pos() {
            let relative_to_center = click_pos.x - response.rect.center().x;
            let value = emath::remap_clamp(relative_to_center / self.inner_length, -0.5..=0.5, 0.0..=1.0);
            self.set_normalized_value(value as f32);
            response.mark_changed();
        }
        if response.double_clicked() {
            self.reset_param();
            response.mark_changed();
        }
        if response.drag_released() {
            self.end_drag();
        }

        if response.hovered() {
            // response doesn't have a method for scroll events,
            // so this goes digging around in ui.input to find them.
            let delta = ui.input(|i| {
                i.events.iter().find_map(|e| match e {
                    Event::Scroll(delta) => Some(*delta),
                    _ => None,
                })
            });
            if let Some(delta) = delta {
                self.setter.begin_set_parameter(self.param);
                self.set_normalized_value(
                    (self.normalized_value() - delta.max_elem() / 100.0).clamp(0.0, 1.0)
                );
                self.setter.end_set_parameter(self.param);
            }
        }

        if ui.is_rect_visible(response.rect) {

            let filled_proportion = self.normalized_value();

            for i in 0..self.inner_length as i32 {
                let center = response.rect.center() +
                    Vec2 { x: i as f32 - self.inner_length / 2.0, y: 0.0 };
                ui.painter().circle_filled(center, self.stripe, Color32::DARK_GRAY);
            }

            let relative_x = emath::remap_clamp(self.normalized_value(), 0.0..=1.0, (-self.inner_length/2.0)..=(self.inner_length/2.0));
            let center = response.rect.center() + vec2(relative_x, 0.0);
            let color = lerp_color(self.low_color, self.high_color, filled_proportion);

            ui.painter().circle_filled(center, self.handle, color);

            ui.painter().text(
                response.rect.left_center() + vec2(35.0, 0.0),
                Align2::RIGHT_CENTER,
                self.low_label.clone(),
                FontId::default(), Color32::GRAY);
            ui.painter().text(
                response.rect.right_center() - vec2(35.0, 0.0),
                Align2::LEFT_CENTER,
                self.high_label.clone(),
                FontId::default(), Color32::GRAY);
        }
    }

}

impl Widget for Slider<'_> {
    fn ui(self, ui: &mut Ui) -> Response {
        let size = vec2(self.outer_length, 20.0);
        let mut response = ui.allocate_response(size, Sense::click_and_drag());
        self.slider_ui(ui, &mut response);
        response
    }
}
