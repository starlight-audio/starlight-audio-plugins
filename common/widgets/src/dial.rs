use std::f32::consts::PI;

use nih_plug::params::FloatParam;
use nih_plug::prelude::{Param, ParamSetter};

use nih_plug_egui::egui::epaint::Hsva;
use nih_plug_egui::egui::{Align2, FontId};
use nih_plug_egui::egui::Color32;
use nih_plug_egui::egui::Vec2;
use nih_plug_egui::egui::{ vec2, Event, Response, Sense, Ui, Widget };
use nih_plug_egui::widgets::util;

use crate::lerp_color;


pub struct Dial<'a> {
    param: &'a FloatParam,
    setter: &'a ParamSetter<'a>,
    size: f32,
    radius: f32,     // radius to center of stripe
    stripe: f32,     // half the width of the stripe
    handle: f32,     // half the width of the handle
    value_font_size: f32,
    label_font_size: f32,
    low_color: Hsva,
    high_color: Hsva
}

impl<'a> Dial<'a> {
    pub fn for_param(param: &'a FloatParam, setter: &'a ParamSetter<'a>) -> Self {
        let size = 180.0;
        let stripe = 2.0;
        let handle = 8.0;
        let radius = size / 2.0 - handle;
        let value_font_size = size / 5.0;
        let label_font_size = size / 6.0;
        let low_color = Hsva::new(0.00, 0.90, 0.72, 1.0);
        let high_color = Hsva::new(0.15, 0.90, 0.95, 1.0);
        Self { param, setter, size, radius, stripe, handle, value_font_size, label_font_size, low_color, high_color }
    }

    fn normalized_value(&self) -> f32 {
        self.param.modulated_normalized_value()
    }

    fn begin_drag(&self) {
        self.setter.begin_set_parameter(self.param);
    }

    fn set_normalized_value(&self, normalized: f32) {
        self.setter.set_parameter_normalized(self.param, normalized)
    }

    /// Begin and end drag still need to be called when using this..
    fn reset_param(&self) {
        self.setter.set_parameter(self.param, self.param.default_plain_value());
    }

    fn end_drag(&self) {
        self.setter.end_set_parameter(self.param);
    }

    fn dial_ui(&self, ui: &Ui, response: &mut Response) {
        if response.drag_started() {
            self.begin_drag();
        }
        if response.dragged() {
            self.set_normalized_value(
                (self.normalized_value() - response.drag_delta().y / self.radius / 3.0).clamp(0.0, 1.0)
            );
            response.mark_changed();
        }
        if response.double_clicked() {
            self.reset_param();
            response.mark_changed();
        }
        if response.drag_released() {
            self.end_drag();
        }

        if response.hovered() {
            // response doesn't have a method for scroll events,
            // so this goes digging around in ui.input to find them.
            let delta = ui.input(|i| {
                i.events.iter().find_map(|e| match e {
                    Event::Scroll(delta) => Some(*delta),
                    _ => None,
                })
            });
            if let Some(delta) = delta {
                self.setter.begin_set_parameter(self.param);
                self.set_normalized_value(
                    (self.normalized_value() - delta.max_elem() / 100.0).clamp(0.0, 1.0)
                );
                self.setter.end_set_parameter(self.param);
            }
        }

        if ui.is_rect_visible(response.rect) {
            let filled_proportion = self.normalized_value();
            let iterations = self.radius as i32 * 2;

            for i in (0..iterations).rev() {
                let proportion = (i as f32) / iterations as f32;
                let color = if proportion <= filled_proportion {
                    let c = lerp_color(self.low_color, self.high_color, proportion);
                    match response.hover_pos() {
                        Some(hover_pos) => {
                            let length_sq = (hover_pos - response.rect.center()).length_sq();
                            if length_sq < self.radius * self.radius {
                                util::add_hsv(c, 0.0, 0.0, 0.2)
                            } else {
                                c
                            }
                        }
                        None => c,
                    }
                } else {
                    Color32::DARK_GRAY
                };

                let angle = 1.25 * PI - proportion * 1.5 * PI;
                let center = response.rect.center() +
                    Vec2 { x: self.radius * angle.cos(), y: - self.radius * angle.sin() };
                ui.painter().circle_filled(center, self.stripe, color);
            }

            let angle = 1.25 * PI - filled_proportion * 1.5 * PI;
            let center = response.rect.center() +
                Vec2 { x: self.radius * angle.cos(), y: - self.radius * angle.sin() };

            let color = lerp_color(self.low_color, self.high_color, filled_proportion);

            ui.painter().circle_filled(center, self.handle, color);
            ui.painter().text(
                response.rect.center(),
                Align2::CENTER_CENTER,
                format!("{}", self.param),
                FontId::proportional(self.value_font_size),
                Color32::GRAY
            );
            ui.painter().text(
                response.rect.center_bottom(),
                Align2::CENTER_BOTTOM,
                self.param.name().to_uppercase(),
                FontId::proportional(self.label_font_size),
                Color32::GRAY
            );
            // ui.painter().rect_stroke(response.rect, 0.0, Stroke::new(1.0, Color32::WHITE));
        }
    }

}

impl Widget for Dial<'_> {
    fn ui(self, ui: &mut Ui) -> Response {
        let desired_size = vec2(self.size, self.size + self.label_font_size);
        let mut response = ui.allocate_response(desired_size, Sense::click_and_drag());
        self.dial_ui(ui, &mut response);
        response
    }
}
