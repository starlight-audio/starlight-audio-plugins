use nih_plug::params::{FloatParam, Param};
use nih_plug_egui::egui::epaint::Hsva;
use nih_plug_egui::egui::{ self, pos2, Color32 };
use nih_plug_egui::egui::{ Response, Sense, Stroke, Ui, Vec2, Widget };
use starlight_audio_dsp::saturation;


pub struct ResponseGraph<'a> {
    drive_param: &'a FloatParam,
    blend_param: &'a FloatParam,
    curve_param: &'a FloatParam,

    low_color: Hsva,
    high_color: Hsva,

    // pixel dimensions
    pub width: f32,    
    pub height: f32
}

impl<'a> ResponseGraph<'a> {

    pub fn new(
        drive_param: &'a FloatParam,
        curve_param: &'a FloatParam,
        blend_param: &'a FloatParam,
        width: f32, height: f32
    ) -> Self {
        let low_color = Hsva::new(0.00, 0.90, 0.72, 1.0);
        let high_color = Hsva::new(0.15, 0.90, 0.95, 1.0);
        Self { drive_param, curve_param, blend_param, low_color, high_color, width, height }
    }

    fn response_graph_ui(&mut self, ui: &mut Ui, response: &mut Response) {
        if ui.is_rect_visible(response.rect) {
            ui.painter().rect_filled(response.rect, 10.0, Color32::BLACK);

            let line_color = Color32::from(Hsva::new(0.00, 0.90, 0.18, 1.0));
            ui.painter().vline(response.rect.center().x, response.rect.y_range(), Stroke::new(1.0, line_color));
            ui.painter().hline(response.rect.x_range(), response.rect.center().y, Stroke::new(1.0, line_color));

            let clipped = ui.painter().with_clip_rect(response.rect);

            let mut saturation_effect = saturation::Saturation::new();

            let mut first = true;
            let mut prev = pos2(0.0,0.0);

            for x in 0..=(response.rect.width() as i32) {
                let input = self.drive_param.modulated_plain_value() *
                    emath::remap(x as f32, 0.0..=response.rect.width(), -1.0..=1.0);
                let gain = saturation_effect.process(input,
                    self.blend_param.modulated_plain_value(),
                    self.curve_param.modulated_plain_value()
                );
                let y = egui::emath::remap(gain * input, 1.0..=-1.0, 0.0..=response.rect.height());

                let next = pos2(response.rect.left() + x as f32, response.rect.top() + y);
                if !first {
                    clipped.line_segment([prev, next], Stroke::new(3.0, self.high_color));
                }
                prev = next;
                first = false;
            }
        }
    }

}

impl Widget for ResponseGraph<'_> {
    fn ui(mut self, ui: &mut Ui) -> Response {
        let mut response = ui.allocate_response(
            Vec2::new(self.width, self.height),
            Sense::click_and_drag()
        );
        self.response_graph_ui(ui, &mut response);
        response
    }
}
