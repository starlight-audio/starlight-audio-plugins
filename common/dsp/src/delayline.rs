///
/// Adapted from the implementation found in Dplug, which was written by
/// Guillaume Piolat and released with the Boost License 1.0
/// https://github.com/AuburnSounds/Dplug/blob/master/dsp/dplug/dsp/delayline.d
///

pub struct DelayLine {
    data: Vec<f32>,
    half: u32, // half the size of the data
    index: u32,
    index_mask: u32,
    num_samples: u32
}

impl DelayLine {

    pub fn new(num_samples: u32) -> Self {
        // Over-allocate to support POW2 delaylines.
        // This wastes memory but allows delay-line of length 0 without tests.
        // The reason to add +1 here is that fundamentally in a delay line of length = 1
        // we want to keep track of the current sample (at delay 0) and the former one (at delay 1).

        let half = (num_samples + 1).next_power_of_two();
        let data = vec![0.0; (half * 2) as usize];
        let index_mask = half - 1;
        let index = index_mask;

        Self { data, half, index, index_mask, num_samples }
    }

    pub fn next_sample(&mut self, input_sample: f32) -> f32 {
        self.feed_sample(input_sample);
        self.sample_full(self.num_samples)
    }

    pub fn feed_sample(&mut self, input_sample: f32) {
        self.index = (self.index + 1) & self.index_mask;
        self.data[self.index as usize] = input_sample;
        self.data[(self.index + self.half) as usize] = input_sample;
    }

    /// Random access sampling of the delay-line at integer points.
    /// Delay 0 = last entered sample with feed().
    fn sample_full(&mut self, delay: u32) -> f32 {
        self.data[(self.index + self.half - delay) as usize]
    }

    pub fn sample_slice(&self, size: usize) -> &[f32] {
        let end: usize = (self.index + self.half).try_into().unwrap();
        let begin = end - size;
        &self.data[begin..end]
    }
}