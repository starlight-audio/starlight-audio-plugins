use crate::biquad;

const C: f32 = 0.5;

pub struct Hysteresis {
    prev_input: f32,
    prev_input_deriv: f32,
    prev_output: f32
}

impl Hysteresis {
    pub fn new() -> Self {
        Self { prev_input: 0.0, prev_input_deriv: 0.0, prev_output: 0.0 }
    }

    pub fn reset(&mut self) {
        self.prev_input = 0.0;
    }

    pub fn process(&mut self, input: f32, sample_rate: f32) -> f32 {
        let t = 1.0 / sample_rate;
        let alpha = 1.6e-3;
        let input_deriv = (((1.0 + 1.0) / t) * (input - self.prev_input)) - self.prev_input_deriv;

        let smoothed_input = (input + self.prev_input) / 2.0;
        let smoothed_input_deriv = (input_deriv + self.prev_input_deriv) / 2.0;

        let diff = smoothed_input + alpha * self.prev_output - self.prev_output;
        let delta = if smoothed_input_deriv > 0.0 { 1.0 } else { -1.0 };
        let delta_m = if (diff > 0.0) == (delta > 0.0) { 1.0 } else { 0.0 };
        let output = self.prev_output + t * ((delta_m * diff / (delta)) * smoothed_input_deriv + C * smoothed_input_deriv);

        self.prev_output = output;
        self.prev_input = input;
        self.prev_input_deriv = input_deriv;

        output
    }
}


pub struct Saturation {
}


impl Saturation {
    pub fn new() -> Self {
        Self { }
    }

    pub fn process(&mut self, input: f32, blend: f32, curve: f32) -> f32 {
        let a = (curve + 1.0) * (curve + 1.0) * (curve + 1.0) - 1.0;
        
        let saturated = if input >= 0.0 {
            1.0 / (1.0 + a * input)
        } else {
            1.0 / (1.0 - emath::lerp(a..=0.0, blend.sqrt()) * input)
        };

        let makeup_gain = emath::lerp((1.0 + a / 4.0)..=1.0, blend);

        makeup_gain * saturated
    }
}
