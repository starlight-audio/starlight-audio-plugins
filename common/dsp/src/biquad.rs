// Adapted from https://www.earlevel.com/main/2012/11/26/biquad-c-source-code/
use std::f32::consts::PI;

use nih_plug::util;

pub const DEFAULT_Q: f32 = 0.70710678;

pub struct BiquadCoefficients {
    a0: f32,
    a1: f32,
    a2: f32,
    b1: f32,
    b2: f32
}

impl BiquadCoefficients {

    pub fn freq_response(self: Self, phi: f32) -> f32 {
        ((self.a0 + self.a1 + self.a2) * (self.a0 + self.a1 + self.a2) -
            4.0 * (self.a0 * self.a1 + 4.0 * self.a0 * self.a2 + self.a1 * self.a2) * phi +
            16.0 * self.a0 * self.a2 * phi * phi
        ).ln() - 
        ((1.0 + self.b1 + self.b2) * (1.0 + self.b1 + self.b2) -
            4.0 * (self.b1 + 4.0 * self.b2 + self.b1 * self.b2) * phi +
            16.0 * self.b2 * phi * phi
        ).ln()
    }

    pub fn identity() -> BiquadCoefficients {
        Self { a0: 1.0, a1: 0.0, a2: 0.0, b1: 0.0, b2: 0.0 }
    }

    pub fn high_pass(sample_rate: f32, freq: f32, q: f32) -> BiquadCoefficients {
        let fc = freq / sample_rate;
        let k = (PI * fc).tan();
        let norm = 1.0 / (1.0 + k / q + k * k);
        let a0 = 1.0 * norm;
        let a1 = -2.0 * a0;
        let a2 = a0;
        let b1 = 2.0 * (k * k - 1.0) * norm;
        let b2 = (1.0 - k / q + k * k) * norm;

        Self { a0, a1, a2, b1, b2 }
    }

    pub fn low_pass(sample_rate: f32, freq: f32, q: f32) -> BiquadCoefficients {
        let fc = freq / sample_rate;
        let k = (PI * fc).tan();
        let norm = 1.0 / (1.0 + k / q + k * k);
        let a0 = k * k * norm;
        let a1 = 2.0 * a0;
        let a2 = a0;
        let b1 = 2.0 * (k * k - 1.0) * norm;
        let b2 = (1.0 - k / q + k * k) * norm;

        Self { a0, a1, a2, b1, b2 }
    }

    pub fn band_pass(sample_rate: f32, freq: f32, q: f32) -> BiquadCoefficients {
        let fc = freq / sample_rate;
        let k = (PI * fc).tan();
        let norm = 1.0 / (1.0 + k / q + k * k);
        let a0 = k / q * norm;
        let a1 = 0.0;
        let a2 = -a0;
        let b1 = 2.0 * (k * k - 1.0) * norm;
        let b2 = (1.0 - k / q + k * k) * norm;

        Self { a0, a1, a2, b1, b2 }
    }

    pub fn notch(sample_rate: f32, freq: f32, q: f32) -> BiquadCoefficients {
        let fc = freq / sample_rate;
        let k = (PI * fc).tan();
        let norm = 1.0 / (1.0 + k / q + k * k);
        let a0 = (1.0 + k * k) * norm;
        let a1 = 2.0 * (k * k - 1.0) * norm;
        let a2 = a0;
        let b1 = a1;
        let b2 = (1.0 - k / q + k * k) * norm;

        Self { a0, a1, a2, b1, b2 }
    }

    pub fn peak(sample_rate: f32, freq: f32, q: f32, gain_db: f32) -> BiquadCoefficients {
        let fc = freq / sample_rate;
        let k = (PI * fc).tan();
        let v = util::db_to_gain(gain_db.abs());
        if gain_db >= 0.0 {  // boost
            let norm = 1.0 / (1.0 + 1.0 / q * k + k * k);
            let a0 = (1.0 + v / q * k + k * k) * norm;
            let a1 = 2.0 * (k * k - 1.0) * norm;
            let a2 = (1.0 - v / q * k + k * k) * norm;
            let b1 = a1;
            let b2 = (1.0 - 1.0 / q * k + k * k) * norm;
            Self { a0, a1, a2, b1, b2 }
        }
        else {  // cut
            let norm = 1.0 / (1.0 + v / q * k + k * k);
            let a0 = (1.0 + 1.0 / q * k + k * k) * norm;
            let a1 = 2.0 * (k * k - 1.0) * norm;
            let a2 = (1.0 - 1.0 / q * k + k * k) * norm;
            let b1 = a1;
            let b2 = (1.0 - v / q * k + k * k) * norm;
            Self { a0, a1, a2, b1, b2 }
        }
    }

    pub fn low_shelf(sample_rate: f32, freq: f32, gain_db: f32) -> BiquadCoefficients {
        let fc = freq / sample_rate;
        let k = (PI * fc).tan();
        let v = util::db_to_gain(gain_db.abs());
        if gain_db >= 0.0 {  // boost
            let norm = 1.0 / (1.0 + 2.0f32.sqrt() * k + k * k);
            let a0 = (1.0 + (2.0 * v).sqrt() * k + v * k * k) * norm;
            let a1 = 2.0 * (v * k * k - 1.0) * norm;
            let a2 = (1.0 - (2.0 * v).sqrt() * k + v * k * k) * norm;
            let b1 = 2.0 * (k * k - 1.0) * norm;
            let b2 = (1.0 - 2.0f32.sqrt() * k + k * k) * norm;
            Self { a0, a1, a2, b1, b2 }
        }
        else {  // cut
            let norm = 1.0 / (1.0 + (2.0 * v).sqrt() * k + v * k * k);
            let a0 = (1.0 + 2.0f32.sqrt() * k + k * k) * norm;
            let a1 = 2.0 * (k * k - 1.0) * norm;
            let a2 = (1.0 - 2.0f32.sqrt() * k + k * k) * norm;
            let b1 = 2.0 * (v * k * k - 1.0) * norm;
            let b2 = (1.0 - (2.0 * v).sqrt() * k + v * k * k) * norm;
            Self { a0, a1, a2, b1, b2 }
        }
    }

    pub fn high_shelf(sample_rate: f32, freq: f32, gain_db: f32) -> BiquadCoefficients {
        let fc = freq / sample_rate;
        let k = (PI * fc).tan();
        let v = util::db_to_gain(gain_db.abs());
        if gain_db >= 0.0 {  // boost
            let norm = 1.0 / (1.0 + 2.0f32.sqrt() * k + k * k);
            let a0 = (v + (2.0 * v).sqrt() * k + k * k) * norm;
            let a1 = 2.0 * (k * k - v) * norm;
            let a2 = (v - (2.0 * v).sqrt() * k + k * k) * norm;
            let b1 = 2.0 * (k * k - 1.0) * norm;
            let b2 = (1.0 - 2.0f32.sqrt() * k + k * k) * norm;
            Self { a0, a1, a2, b1, b2 }
        }
        else {  // cut
            let norm = 1.0 / (v + (2.0 * v).sqrt() * k + k * k);
            let a0 = (1.0 + 2.0f32.sqrt() * k + k * k) * norm;
            let a1 = 2.0 * (k * k - 1.0) * norm;
            let a2 = (1.0 - 2.0f32.sqrt() * k + k * k) * norm;
            let b1 = 2.0 * (k * k - v) * norm;
            let b2 = (v - (2.0 * v).sqrt() * k + k * k) * norm;
            Self { a0, a1, a2, b1, b2 }
        }
    }
}

pub struct Biquad {
    z1: f32,
    z2: f32
}

impl Biquad {
    pub fn new() -> Self {
        Self { z1: 0.0, z2: 0.0 }
    }

    pub fn process(&mut self, input: f32, coefficients: &BiquadCoefficients) -> f32 {
        let out = input * coefficients.a0 + self.z1;
        self.z1 = input * coefficients.a1 + self.z2 - coefficients.b1 * out;
        self.z2 = input * coefficients.a2 - coefficients.b2 * out;
        out
    }

}
