pub mod biquad;
pub mod delayline;
pub mod oversampling;
pub mod saturation;
pub mod spectrum;

pub const fn oversampling_factor_to_times(factor: usize) -> usize {
    2usize.pow(factor as u32)
}

pub const fn oversampling_times_to_factor(times: usize) -> usize {
    times.ilog2() as usize
}
